#!/bin/bash

while [ -n "$1" ]; do
  case "$1" in
  -d)
    domain="$2"
    IFS=' ' read -r -a domains <<<"$2"
    shift
    ;;
  -e)
    email="$2"
    shift
    ;;
  -r)
    route="$2"
    shift
    ;;
  --)
    shift
    break
    ;;
  *) echo "$1 is not an option" ;;
  esac
  shift
done



rsa_key_size=4096
data_path="./data/certbot"
staging=0 # Set to 1 if you're testing your setup to avoid hitting request limits

function createHostConfig() {

    sed -e "s/domain/$1/g; s/routePath/$2/g"  ./templates/host.conf > ./nginx_main/"$1".conf

}

function createTempSsl() {
  echo "### Creating dummy certificate for $1 ..."
  path="/etc/letsencrypt/live/$1"
  mkdir -p "$data_path/conf/live/$1"
  docker-compose run --rm --entrypoint "\
      openssl req -x509 -nodes -newkey rsa:$rsa_key_size -days 1\
        -keyout '$path/privkey.pem' \
        -out '$path/fullchain.pem' \
        -subj '/CN=localhost'" certbot
  echo

}

function removeTempSsl() {
  echo "### Deleting dummy certificate for $1 ..."
  docker-compose run --rm --entrypoint "\
      rm -Rf /etc/letsencrypt/live/$1 && \
      rm -Rf /etc/letsencrypt/archive/$1 && \
      rm -Rf /etc/letsencrypt/renewal/$1.conf" certbot
  echo
}

createSsl() {
  echo "### Requesting Let's Encrypt certificate for $1 ..."

  # Select appropriate email arg
  case "$email" in
  "") email_arg="--register-unsafely-without-email" ;;
  *) email_arg="--email $email" ;;
  esac

  # Enable staging mode if needed
  if [ $staging != "0" ]; then staging_arg="--staging"; fi

  docker-compose run --rm --entrypoint "\
    certbot certonly --webroot -w /var/www/certbot \
       $staging_arg \
      -d $1 \
      $email_arg \
      --rsa-key-size $rsa_key_size \
      --agree-tos \
      --force-renewal" certbot
  echo
  echo "### Reloading nginx ..."
  docker-compose exec nginx_main nginx -s reload

}

if ! [ -x "$(command -v docker-compose)" ]; then
  echo 'Error: docker-compose is not installed.' >&2
  exit 1
fi

if [ ! -e "$data_path/conf/options-ssl-nginx.conf" ] || [ ! -e "$data_path/conf/ssl-dhparams.pem" ]; then
  echo "### Downloading recommended TLS parameters ..."
  mkdir -p "$data_path/conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf >"$data_path/conf/options-ssl-nginx.conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/certbot/ssl-dhparams.pem >"$data_path/conf/ssl-dhparams.pem"
  echo
fi

for domain in "${domains[@]}"; do
  echo "Domain is: $domain"
  createTempSsl "$domain"
  createHostConfig $domain $route
done

echo "### Starting nginx ..."
docker-compose up -d --force-recreate
echo

for domain in "${domains[@]}"; do
  echo "Domain is: $domain"
  removeTempSsl $domain
  createSsl $domain
done
